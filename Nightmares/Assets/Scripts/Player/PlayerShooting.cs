﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public int pellets = 5;
    public int accuracy = 100;
    public float cooldowntimer = 3f;
    public float timeBetweenBullets = 0.15f;
    private float timeBetweenBulletsBase = .15f;
    public float timeBetweenPellets = .9f;
    public float range = 100f;


    float timer;
    float shotguntimer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;

    void Awake()
    {
        shootableMask = LayerMask.GetMask("Shootable");
        gunParticles = GetComponent<ParticleSystem>();
        gunLine = GetComponent<LineRenderer>();
        gunAudio = GetComponent<AudioSource>();
        gunLight = GetComponent<Light>();
    }


    void Start()
    {
        StartCoroutine(Cooldown());
    }


    void Update ()
    {
        timer += Time.deltaTime;
        shotguntimer += Time.deltaTime;

        if (timeBetweenBullets <= timeBetweenBulletsBase)
        {
            timeBetweenBullets = timeBetweenBulletsBase;
        }


        if (Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            Shoot ();
        }

        if (Input.GetButton("Fire2") && shotguntimer >= timeBetweenPellets && Time.timeScale != 0)
        {
            ShotgunShoot();
        }

        if (timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }

        if (timer >= timeBetweenPellets * effectsDisplayTime)
        {
            DisableEffects();
        }
    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot ()
    {
        timer = 0f;
        timeBetweenBullets += .009f;
        cooldowntimer = 0f;

        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if (Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }

    void ShotgunShoot()
    {
        shotguntimer = 0f;

        gunAudio.Play();

        gunLight.enabled = true;

        gunParticles.Stop();
        gunParticles.Play();

        gunLine.enabled = true;
        gunLine.SetPosition(0, transform.position);

        shootRay.origin = transform.position;
        for (var i = 0; i < pellets; i++)
        {
            var offset = Random.insideUnitSphere * accuracy;
            offset.y = 0;
            shootRay.direction = transform.forward + offset;

            if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
            {
                EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
                if (enemyHealth != null)
                {
                    enemyHealth.TakeDamage(damagePerShot, shootHit.point);
                }
                gunLine.SetPosition(1, shootHit.point);
            }
            else
            {
                gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
            }
        }
    }

    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(cooldowntimer);
        timeBetweenBullets -= .0004f;
        StartCoroutine(Cooldown());
    }
}

